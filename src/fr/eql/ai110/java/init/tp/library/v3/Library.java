package fr.eql.ai110.java.init.tp.library.v3;

import java.util.Scanner;

public class Library {

	Reader reader = null;
	Scanner myScanner;
	String input = "";
	ReaderDAO readerDAO = new ReaderDAO();
	
	public Library() {
		
	}

	public void executeProgram() {
		
		myScanner = new Scanner(System.in);
		
		System.out.println("**************************");
		System.out.println("* BIENVENUE DANS iBIBLIO *");
		System.out.println("**************************");
		
		/*
		 * Je boucle sur les menus utilisateur tant que l'utilisateur ne souhaite
		 * pas quitter l'application.
		 */
		do {
			registerReader();
			menuSelection();
			
			switch (input) {
			case "i" :
				displayIdentity();
				break;
			case "e" :
				registerBooks();
				break;
			case "l" :
				displayBooks();
				break;
			default:
				break;
			}
		} while (!input.equals("q"));
		myScanner.close();
		System.out.println("\r\nFin du programme.");
	}
	
	private void registerReader() {
		if (reader == null) {
			String login;
			String password;
			// On boucle jusqu'à ce qu'un reader enregistré se connecte.
			do {
				System.out.println("Créer un compte (c)");
				System.out.println("Se Logger (l)");
				input = myScanner.nextLine();
				switch (input) {
				case "c":
					System.out.println("\r\nPour commencer, veuillez renseigner votre identité.");
					System.out.println("Prénom : ");
					String name = myScanner.nextLine();
					System.out.println("Nom : ");
					String surname = myScanner.nextLine();
					System.out.println("Adresse : ");
					String address = myScanner.nextLine();
					System.out.println("Login : ");
					login = myScanner.nextLine();
					System.out.println("Password : ");
					password = myScanner.nextLine();
					
					/* Si la méthode createAccount retourne false, le fichier utilisateur
					 * n'a pas été créé car un autre portant le même nom (correpondant au
					 * login entré) existe déjà.
					 */
					boolean isCreated = readerDAO.createAccount(name, surname, address, login, password);
					if (!isCreated) {
						System.out.println("Ce login n'est pas diponible.\r\nChoisissez-en un autre.");
					}
					break;
				case "l":
					System.out.println("Login : ");
					login = myScanner.nextLine();
					System.out.println("Password : ");
					password = myScanner.nextLine();
					reader = readerDAO.connect(login, password);
					if (reader != null) {
						System.out.println("Heureux de vous revoir " 
								+ reader.getName()
								+ " " 
								+ reader.getSurname() + " !");
					} else {
						System.out.println("Login et/ou password incorrect(s) !");
					}
					break;
				default:
					break;
				}
			} while (reader == null);
		}
	}
	
	private void menuSelection() {
		/*
		 * Menu
		 */
		System.out.println("\r\n*** MENU ***");
		System.out.println("\tAfficher mon identité (i)");
		System.out.println("\tEnregistrer des livres (e)");
		if (!reader.getBooks().isEmpty()) {
			System.out.println("\tAfficher mes livres (l)");
		}
		System.out.println("\tQuitter l'application (q)");
		input = myScanner.nextLine();
	}
	
	private void displayIdentity() {
		System.out.println("\r\n*** Afficher mon identité ***");
		System.out.println("\tPrénom : " + reader.getName() 
										+ "\r\n\tNom : " + reader.getSurname()
										+ "\r\n\tAdresse : " + reader.getAddress());
	}
	
	private void registerBooks() {
		System.out.println("\r\n*** Enregistrer des livres ***");
		System.out.println("Combien souhaitez-vous enregistrer de livres ?");
		int nbBooks = Integer.parseInt(myScanner.nextLine());
		for (int i = 0; i < nbBooks; i++) {
			System.out.println("Saisissez les informations du livre " + (i + 1));
			System.out.println("Titre : ");
			String title = myScanner.nextLine();
			System.out.println("Auteur : ");
			String author = myScanner.nextLine();
			System.out.println("ISBN : ");
			int isbn = Integer.parseInt(myScanner.nextLine());
			System.out.println("Catégorie parmi les suivantes : ");
			Category[] categories = Category.values();
			for (Category c : categories) {
				System.out.println("\t" + c.toString().toLowerCase());
			}
			Category category = Category.valueOf(myScanner.nextLine().toUpperCase());
			Book book = new Book(title, author, isbn, category);
			reader.getBooks().add(book);
		}
	}
	
	private void displayBooks() {
		System.out.println("\r\n*** Liste de mes livres ***");
		for (Book b : reader.getBooks()) {
			System.out.println("\r\n\tTitre : " + b.getTitle() 
								+ "\r\n\tAuteur : " + b.getAuthor()
								+ "\r\n\tISBN : " + b.getIsbn()
								+ "\r\n\tCatégorie : " + b.getCategory());
		}
	}
}
