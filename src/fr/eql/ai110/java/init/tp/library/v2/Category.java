package fr.eql.ai110.java.init.tp.library.v2;

public enum Category {
	LITTERATURE, HISTOIRE, SF, ANTHROPOLOGIE
}
