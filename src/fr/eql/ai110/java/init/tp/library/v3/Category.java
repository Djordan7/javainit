package fr.eql.ai110.java.init.tp.library.v3;

public enum Category {
	LITTERATURE, HISTOIRE, SF, ANTHROPOLOGIE
}
