package fr.eql.ai110.java.init.tp.library.v3;

import java.util.ArrayList;
import java.util.List;

public class Reader {

	private String name;
	private String surname;
	private String address;
	private String login;
	private String password;
	private List<Book> books = new ArrayList<>();
	
	public Reader(String name, String surname, String address, String login, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.address = address;
		this.login = login;
		this.password = password;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<Book> getBooks() {
		return books;
	}
	public void setBooks(List<Book> books) {
		this.books = books;
	}
}
