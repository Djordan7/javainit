package fr.eql.ai110.java.init.tp.library.v3;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReaderDAO {

	public boolean createAccount(String name, String surname,
			String address, String login, String password) {
		
		File folder = new File("Library");
		// Si le dossier n'existe pas, il sera créé.
		if (!folder.exists()) {
			folder.mkdir();
		}
		boolean isCreated;
		// Je déclare le fichier utilisateur.
		File userFile = new File(folder + "/" + login + ".lib");
		try {
			// Je tente de créer le fichier sur le disque, s'il n'existe pas déjà.
			isCreated = userFile.createNewFile();
			// Si le fichier a été créé, alors il s'agit d'un nouveau reader.
			if (isCreated) {
				FileWriter out = new FileWriter(userFile, false);
				BufferedWriter bw = new BufferedWriter(out);
				// J'écris dans mon fichier.
				bw.write(name);
				bw.newLine();
				bw.write(surname);
				bw.newLine();
				bw.write(address);
				bw.newLine();
				bw.write(login);
				bw.newLine();
				bw.write(password);
				bw.close();
				out.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
			isCreated = false;
		}
		return isCreated;
	}
	
	public Reader connect(String login, String password) {
		File userFile = new File("Library/" + login + ".lib");
		Reader reader = null;
		// Si l'utilisateur n'existe pas, on retourne null.
		if (!userFile.exists()) {
			return null;
		} else {
			try {
				// S'il existe, on va lire ses informations dans le fichier correpondant.
				FileReader in = new FileReader(userFile);
				BufferedReader br = new BufferedReader(in);
				String nameInFile = br.readLine();
				String surnameInFile = br.readLine();
				String addressInFile = br.readLine();
				String loginInFile = br.readLine();
				String passwordInFile = br.readLine();
				reader = new Reader(nameInFile, surnameInFile, addressInFile, loginInFile, passwordInFile);
				br.close();
				in.close();	
			} catch (IOException e) {
				e.printStackTrace();
			}
			// Si le password est correct, on retourne l'instance de reader.
			if (reader.getPassword().equals(password)) {
				return reader;
			// Autrement, on retourne null.
			} else {
				return null;
			}
		}
	}
}
