package fr.eql.ai110.java.init.tp.library.v1;

public class Book {

	private String title;
	private String author;
	private int isbn;
	private Category category;
	
	public Book(String title, String author, int isbn, Category category) {
		super();
		this.title = title;
		this.author = author;
		this.isbn = isbn;
		this.category = category;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getIsbn() {
		return isbn;
	}
	public void setIsbn(int isbn) {
		this.isbn = isbn;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
}
