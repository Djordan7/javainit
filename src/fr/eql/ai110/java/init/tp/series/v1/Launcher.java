package fr.eql.ai110.java.init.tp.series.v1;

import java.util.ArrayList;
import java.util.List;

public class Launcher {

	public static void main(String[] args) {
		
		Episode episode1 = new Episode("Nice city", "Robert Noname");
		Episode episode2 = new Episode("Lousy city", "John Coolname");
		Episode episode3 = new Episode("Hello Sue-Hellen", "Robert Noname");
		Episode episode4 = new Episode("Petrol !", "Robert Noname");
		Episode episode5 = new Episode("Houston isn't here", "Suzanne Strange");
		Episode episode6 = new Episode("All the money", "Mister T");
		
		List<Episode> season1Episodes = new ArrayList<>();
		season1Episodes.add(episode1);
		season1Episodes.add(episode2);
		season1Episodes.add(episode3);
		
		List<Episode> season2Episodes = new ArrayList<>();
		season2Episodes.add(episode4);
		season2Episodes.add(episode5);
		season2Episodes.add(episode6);
		
		Season season1 = new Season(1, season1Episodes);
		Season season2 = new Season(2, season2Episodes);
		
		List<Season> seasons = new ArrayList<>();
		seasons.add(season1);
		seasons.add(season2);
		
		Series dallas = new Series(
				"Dallas", 
				1978, 
				"Un univers impitoyable.", 
				seasons
				);
		
		dallas.displaySeries();
		
		dallas.displayCategory("Titre");
		dallas.displayCategory("Année");
		dallas.displayCategory("Synopsis");
		dallas.displayCategory("Toto");
		
		dallas.changeTitle("Houston");
		
		System.out.println("\r\n### Episodes par auteur et saison ###");
		dallas.displayWriterEpisodes("Robert Noname");
	}
}
