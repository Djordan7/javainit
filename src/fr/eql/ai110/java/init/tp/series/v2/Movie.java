package fr.eql.ai110.java.init.tp.series.v2;

public class Movie {

	private String movieName;
	private String writer;
	
	public Movie(String movieName, String writer) {
		super();
		this.movieName = movieName;
		this.writer = writer;
	}
	
	public String getMovieName() {
		return movieName;
	}
	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
}
