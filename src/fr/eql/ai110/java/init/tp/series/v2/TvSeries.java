package fr.eql.ai110.java.init.tp.series.v2;

import java.util.List;

import fr.eql.ai110.java.init.tp.series.v2.Episode;
import fr.eql.ai110.java.init.tp.series.v2.Season;

public class TvSeries extends Series{

	private List<Season> seasons;
	
	public TvSeries(String title, int releaseYear, String synopsis, List<Season> seasons) {
		super(title, releaseYear, synopsis);
		this.seasons = seasons;
	}

	@Override
	public void displayWriter(String writer) {
		System.out.println("Episodes écrits par " + writer + " :");
		for (Season s : seasons) {
			System.out.println("*** Saison " + s.getSeasonNumber() + " ***");
			for (Episode e : s.getEpisodes()) {
				if (e.getWriter().equals(writer)) {
					System.out.println(e.getTitle());
				}
			}
		}
		
	}
}
