package fr.eql.ai110.java.init.tp.series.v2;

import java.util.List;

import fr.eql.ai110.java.init.tp.series.v1.Episode;
import fr.eql.ai110.java.init.tp.series.v1.Season;

public abstract class Series {

	private String title;
	private int releaseYear;
	private String synopsis;
	
	public Series(
			String title, 
			int releaseYear, 
			String synopsis
			) {
		this.title = title;
		this.releaseYear = releaseYear;
		this.synopsis = synopsis;
	}
	
	public void displaySeries() {
		System.out.println("La série s'appelle : " + title
				+ "\r\nL'année de sortie est : " + releaseYear 
				+ "\r\nSon histoire : " + synopsis
				+ "\r\n");
	}
	
	public void displayCategory(String category) {
		switch (category) {
		case "Titre" :
			System.out.println("Voici le titre : " + title);
			break;
		case "Année" :
			System.out.println("Voici l'année de sortie : " + releaseYear);
			break;
		case "Synopsis" :
			System.out.println("Voici le synopsis : " + synopsis);
			break;
		default:
			System.out.println("Aucun élément sous la catégorie '" + category + "'.");
		}
		System.out.println();
	}
	
	public void changeTitle(String title) {
		String oldTitle = this.title;
		this.title = title;
		System.out.println("Le titre a été changé de '" + oldTitle + "' à '" + this.title + "'.");
	}
	
	public abstract void displayWriter(String writer);
}
