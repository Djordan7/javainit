package fr.eql.ai110.java.init.tp.series.v1;

public class Episode {
	
	private String title;
	private String writer;
	
	public Episode(String title, String writer) {
		this.title = title;
		this.writer = writer;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
}
