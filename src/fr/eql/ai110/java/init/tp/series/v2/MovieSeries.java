package fr.eql.ai110.java.init.tp.series.v2;

import java.util.List;

import fr.eql.ai110.java.init.tp.series.v1.Episode;
import fr.eql.ai110.java.init.tp.series.v1.Season;

public class MovieSeries extends Series {

	List<Movie> movies;
	
	public MovieSeries(String title, int releaseYear, String synopsis, List<Movie> movies) {
		super(title, releaseYear, synopsis);
		this.movies = movies;
	}

	@Override
	public void displayWriter(String writer) {
		System.out.println("Films écrits par " + writer + " :");
		for (Movie m : movies) {
			if (m.getWriter().equals(writer)) {
				System.out.println("\r\n" + m.getMovieName());
			}
		}
	}

}
