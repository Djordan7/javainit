package fr.eql.ai110.java.init.tp.series.v1;

import java.util.List;

public class Series {

	private String title;
	private int releaseYear;
	private String synopsis;
	private List<Season> seasons;
	
	public Series(
			String title, 
			int releaseYear, 
			String synopsis, 
			List<Season> seasons
			) {
		this.title = title;
		this.releaseYear = releaseYear;
		this.synopsis = synopsis;
		this.seasons = seasons;
	}
	
	public void displaySeries() {
		System.out.println("La série s'appelle : " + title
				+ "\r\nL'année de sortie est : " + releaseYear 
				+ "\r\nSon histoire : " + synopsis
				+ "\r\n");
	}
	
	public void displayCategory(String category) {
		switch (category) {
		case "Titre" :
			System.out.println("Voici le titre : " + title);
			break;
		case "Année" :
			System.out.println("Voici l'année de sortie : " + releaseYear);
			break;
		case "Synopsis" :
			System.out.println("Voici le synopsis : " + synopsis);
			break;
		default:
			System.out.println("Aucun élément sous la catégorie '" + category + "'.");
		}
		System.out.println();
	}
	
	public void changeTitle(String title) {
		String oldTitle = this.title;
		this.title = title;
		System.out.println("Le titre a été changé de '" + oldTitle + "' à '" + this.title + "'.");
	}
	
	public void displayWriterEpisodes(String writer) {
		System.out.println("Episodes écrits par " + writer + " :");
		for (Season s : seasons) {
			System.out.println("*** Saison " + s.getSeasonNumber() + " ***");
			for (Episode e : s.getEpisodes()) {
				if (e.getWriter().equals(writer)) {
					System.out.println(e.getTitle());
				}
			}
		}
	}
}
