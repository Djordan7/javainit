package fr.eql.ai110.java.init.demo.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DemoDate {

	public static void main(String[] args) {
		
		/*
		 * Instancier la date du jour
		 */
		Date date = new Date();
		
		/*
		 * Pour afficher une date, 2 méthodes.
		 */
		// 1
		System.out.println("Date du jour : " + date.toString());
		
		// 2
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
		System.out.println("Date du jour formatée : " + sdf.format(date));
		
		/*
		 * Convertir une chaîne de caractères en date
		 */
		String dateToFormat = "14-10-2021 16:38:20";
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		try {
			Date today = sdf2.parse(dateToFormat);
			System.out.println(today);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Comparer des java.util.Date
		 */
		
		/*
		 * Méthode 1
		 */
		try {
			Date date1 = sdf.parse("14/10/2021");
			Date date2 = sdf.parse("12/08/1992");
			
			if (date1.after(date2)) {
				System.out.println("date1 plus récente que date2.");
			} else if (date1.before(date2)) {
				System.out.println("date1 plus ancienne que date2.");
			} else if (date1.equals(date2) ) {
				System.out.println("Les date sont identiques.");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * Méthode 2
		 */
		
		try {
			Date date1 = sdf.parse("14/10/2021");
			Date date2 = sdf.parse("12/08/1992");
			
			if (date1.compareTo(date2) > 0) {
				System.out.println("date1 plus récente que date2.");
			} else if (date1.compareTo(date2) < 0) {
				System.out.println("date1 plus ancienne que date2.");
			} else if (date1.compareTo(date2) == 0 ) {
				System.out.println("Les date sont identiques.");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
