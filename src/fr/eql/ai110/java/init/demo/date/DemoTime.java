package fr.eql.ai110.java.init.demo.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DemoTime {

	public static void main(String[] args) {
		
		// LocalDate, LocalTime, LocalDateTime, ZoneDateTime
		
		/*
		 * Date d'aujourd'hui.
		 */
		LocalDate todaysDate = LocalDate.now();
		System.out.println("La date du jour : " + todaysDate);
		
		/*
		 * Date précise.
		 */
		LocalDate someDate = LocalDate.of(2021, 1, 25);
		System.out.println("Une date : " + someDate);
		
		/*
		 * Modifier une date
		 */
		LocalDate modifiedDate = someDate.withYear(1998).withMonth(12);
		System.out.println("Date modifiée : " + modifiedDate);
		
		/*
		 * Convertir une chaîne de caractères en date
		 */
		LocalDate dateFromString = 
				LocalDate.parse("2018-02-03", DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		System.out.println("Date depuis String : " + dateFromString);
		
		/*
		 * Incrémenter ou décrémenter la date.
		 */
		LocalDate incrementedDate = dateFromString.plusDays(32);
		System.out.println("Date depuis Sting incrémentée de 32 jours : " + incrementedDate);
		
		/*
		 * Heure sans la date
		 */
		LocalTime now = LocalTime.now();
		System.out.println("L'heure actuelle : " + now);
		
		LocalTime parsedTime = LocalTime.parse("17:20");
		System.out.println("Une heure parsée : " + parsedTime );
		
		LocalTime newTime = parsedTime.plusMinutes(45);
		System.out.println("Heure changée : " + newTime);
		
		/*
		 * ZonedDateTime
		 */
		ZonedDateTime zonedDateTime = ZonedDateTime.of(LocalDateTime.now(), ZoneId.systemDefault());
		System.out.println(zonedDateTime);
		
		ZonedDateTime australiaSydneyZonedDateTime = ZonedDateTime.of(LocalDateTime.now(), 
				ZoneId.of("Australia/Sydney"));
		System.out.println(australiaSydneyZonedDateTime);
	}
}
