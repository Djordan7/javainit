package fr.eql.ai110.java.init.demo.string;

public class DemoString {

	public static void main(String[] args) {
		
		String sentence = "Hello world";
		String emptySentence = "";
		
		System.out.println("*** Méthodes de String ***");
		
		System.out.println("\r\n--- Obtenir la taille "
				+ "(nombre de caractères) ---");
		System.out.println("sentence : " + sentence.length());
		System.out.println("emptySentence : " + emptySentence.length());
		
		System.out.println("\r\n--- Vérifier si la chaîne est vide ---");
		System.out.println("sentence : " + sentence.isEmpty());
		System.out.println("emptySentence : " + emptySentence.isEmpty());
		
		System.out.println("\r\n--- Obtenir une partie de la chaîne ---");
		System.out.println("sentence (à partir du caractère à "
				+ "l'index 4 inclus :" + sentence.substring(4));
		System.out.println("sentence (à partir du caractère à "
				+ "l'index 4 inclus jusqu'au caractère à"
				+ "l'index 7 exclus :" + sentence.substring(4, 7));
		
		System.out.println("\r\n--- Retirer les espaces entourant la chaîne ---");
		String sentenceWithSurroundingSpaces = "       toto      ";
		System.out.println("sentenceWithSurroundingSpaces : " 
				+ sentenceWithSurroundingSpaces.length());
		String trimmedSentence = sentenceWithSurroundingSpaces.trim();
		System.out.println("trimmedSentence : " 
				+ trimmedSentence.length());
		
		System.out.println("\r\n--- Découper la chaîne en plusieurs partie, "
				+ "selon un séparateur ---");
		String[] sentenceParts = sentence.split(" ");
		System.out.println("Partie de 'Hello World' avant l'espace : '" 
				+ sentenceParts[0] +  "'");
		System.out.println("Partie de 'Hello World' après l'espace : '" 
				+ sentenceParts[1] +  "'");
		
		System.out.println("\r\n--- Remplacer une partie d'une chaîne par"
				+ " une autre chaîne ---");
		String replacedSentence = sentence.replace("world", "Toto");
		System.out.println(replacedSentence);
		
		System.out.println("\r\n*** StringBuffer ***");
		
		String part1 = "Hello";
		String part2 = "World";
		String part3 = "!";
		String allParts = part1 + " " + part2 + " " + part3;
		System.out.println("Concaténation classique : " + allParts);
		
		/*
		 * Un StringBuffer (ou StringBuilder) permet de concaténer
		 * des chaînes de caractères de manière plus performante
		 * qu'une concaténation classique.
		 */
		
		StringBuffer myBuffer = new StringBuffer();
		myBuffer
			.append(part1) // pour rajouter à la fin de la chaîne.
			.append(part2)
			.append(part3)
			.insert(5, " ") // pour insérer à une position (indice) donnée.
			.insert(11, " ");
		
		allParts = myBuffer.toString();
		System.out.println("Concaténation par StringBuffer : " + allParts);
		
		System.out.println("\r\n### Comparaisons entre deux chaînes "
				+ "de caractères identiques ###");
		
		System.out.println("\r\n**** Entre deux litéraux ***");
		String a = "toto";
		String b = "toto";
		System.out.println("--- Objets ---");
		System.out.println(a == b);
		System.out.println("--- Contenus ---");
		System.out.println(a.equals(b));
		
		System.out.println("\r\n**** Entre deux objets construits ***");
		String c = new String("tata");
		String d = new String("tata");
		System.out.println("--- Objets ---");
		System.out.println(c == d);
		System.out.println("--- Contenus ---");
		System.out.println(c.equals(d));
		
		System.out.println("\r\n**** Entre un litéral et un objet construit ***");
		String e = "titi";
		String f = new String("titi");
		System.out.println("--- Objets ---");
		System.out.println(e == f);
		System.out.println("--- Contenus ---");
		System.out.println(e.equals(f));
	}
}
