package fr.eql.ai110.java.init.demo.copy;

import fr.eql.ai110.java.init.demo.oop.Dog;

public class DemoCopy {

	public static void main(String[] args) {
		
		System.out.println("*** Copie par valeur ***");
		int number1 = 5;
		int number2 = number1;
		System.out.println("number1 : " + number1);
		System.out.println("number2 : " + number2);
		number1 = 6;
		System.out.println("number1 : " + number1);
		System.out.println("number2 : " + number2);
		
		System.out.println("\r\n*** Copie par référence ***");
		Dog monChien = new Dog();
		
		monChien.setName("Fido");
		Dog copieDeMonChien = monChien;
		System.out.println("monChien : " + monChien.getName());
		System.out.println("copieDeMonChien : " + copieDeMonChien.getName());
		
		monChien.setName("Pas Fido");
		System.out.println("monChien : " + monChien.getName());
		System.out.println("copieDeMonChien : " + copieDeMonChien.getName());
		
		copieDeMonChien.setName("Milou");
		System.out.println("monChien : " + monChien.getName());
		System.out.println("copieDeMonChien : " + copieDeMonChien.getName());
	}
}
