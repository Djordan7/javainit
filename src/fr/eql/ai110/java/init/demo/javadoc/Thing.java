package fr.eql.ai110.java.init.demo.javadoc;

/*
 * Pour générer la javadoc :
 * Se placer dans le répertoire de destination de la javadoc.
 * Y ouvrir une invite de commande.
 * Taper javadoc C:\chemin\complet\vers\Thing.java
 * Ouvrir le fichier index.html généré.
 */

/**
 * It's a thing.
 * 
 * @author akatz
 *
 */
public class Thing {

	/**
	 * Thats a feature.
	 */
	private String feature;

	/**
	 * Allows the building of a thing.
	 * @param feature The feature needed to build the thing
	 */
	public Thing(String feature) {
		this.feature = feature;
	}

	/**
	 * It does something.
	 * @param tool The tool do do the thing
	 * @return True if all goes well
	 */
	public boolean doThing(String tool) {
		return true;
	}
	
	/**
	 * Get the feature of the thing.
	 * @return The feature
	 */
	public String getFeature() {
		return feature;
	}
	/**
	 * Sets the feature.
	 * @param feature The feature to set
	 */
	public void setFeature(String feature) {
		this.feature = feature;
	}
}
