package fr.eql.ai110.java.init.demo.log4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DemoLog4j {

	/*
	 * Les "Logger" et "LogManager" d'Apache ne font pas partie des
	 * librairies standard de Java.
	 * Il faut donc rajouter les 2 archives java (.jar) dans les
	 * librairies utilisées par ce projet.
	 * Sous Eclipse :
	 * "Properties" du projet > "Java Build Path" > Onglet "Librairies" > Bouton "Add External JARs"
	 * Ajouter les 2 jar de log4j :
	 * "log4j-api-XXX.jar" et "log4j-core-XXX.jar"
	 */
	private final static Logger logger = LogManager.getLogger();
	
	public static void main(String[] args) {
		
		/*
		 * Niveau des messages affichés, chaque niveau incluant les niveaux inférieurs.
		 * ALL < DEBUG < INFO < WARN < ERROR < FATAl < OFF
		 */
		// Messages pour les développeurs
		logger.debug("Un log utile uniquement pendant la phase de développement");
		// Messages pour les utilisateurs
		logger.info("Un log informatif");
		logger.warn("Un log d'alerte pas trop méchant");
		logger.error("Un log pour signaler une erreur qui n'a pas fait crasher l'application");
		logger.fatal("Un log qui signale une erreur fatale susceptible de faire crasher l'application");
	}
}
