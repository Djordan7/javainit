package fr.eql.ai110.java.init.demo.collection;

import java.util.*;

public class DemoIterator {
	
	public static void main(String[] args) {

		List<Integer> integers;

		System.out.println("Retirer les 0 de la liste de départ :");
		integers = new ArrayList<>();
		Collections.addAll(integers, 27, 0, 0, 35, 52);
		System.out.println("\t" + integers);

		System.out.println("Avec une boucle while (pas d'itération sur la liste) :");
		integers = new ArrayList<>();
		Collections.addAll(integers, 27, 0, 0, 35, 52);
		while (integers.contains(0)) {
			integers.remove(new Integer(0));
		}
		System.out.println("\t" + integers);

		System.out.println("Avec une boucle for (itération sur la liste) :");
		integers = new ArrayList<>();
		Collections.addAll(integers, 27, 0, 0, 35, 52);
		for (int i = 0; i < integers.size(); i++) {
			if (integers.get(i) == 0) {
				integers.remove(i);
			}
		}
		System.out.println("\t" + integers);

		System.out.println("Avec une boucle foreach (itération sur la liste) :");
		try {
			integers = new ArrayList<>();
			Collections.addAll(integers, 27, 0, 0, 35, 52);
			int index = 0;
			for (Integer integer : integers) {
				if (integer == 0) {
					integers.remove(index);
				}
				index++;
			}
			System.out.println("\t" + integers);
		} catch (ConcurrentModificationException e) {
			System.out.println("\tOpération impossible dans un foreach.");
		}

		System.out.println("Avec un Iterator :");
		integers = new ArrayList<>();
		Collections.addAll(integers, 27, 0, 0, 35, 52);
		Iterator<Integer> integersIterator = integers.iterator();
		while (integersIterator.hasNext()) {
			if (integersIterator.next() == 0) {
				integersIterator.remove();
			}
		}
		// Version lambda des 4 lignes précédentes :
		// integers.removeIf(integer -> integer == 0);
		System.out.println("\t" + integers);
	}
}
