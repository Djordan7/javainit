package fr.eql.ai110.java.init.demo.collection;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fr.eql.ai110.java.init.demo.enums.DogRace;
import fr.eql.ai110.java.init.demo.oop.Dog;
import fr.eql.ai110.java.init.demo.oop.Toy;

public class DemoHashMap {

	public static void main(String[] args) {
		
		/*
		 * Une HashMap permet de créer des couples clé/valeurs dans une collection.
		 * La clé doit être unique.
		 */
		
		/*
		 * Déclaration d'une HashMap.
		 */
		Map<String, Integer> quantityByFruit = new HashMap<String, Integer>();
		
		/*
		 * Ajout de couples clé/valeur dans le Map avec la méthode put()
		 */
		quantityByFruit.put("Kiwi", 6);
		quantityByFruit.put("Citron", 2);
		quantityByFruit.put("Banane", 4);
		
		/*
		 * Récupération d'une valeur de la map avec la méthode get(),
		 * qui prend en paramètre la valeur d'une clé.
		 */
		System.out.println("Quantité de bananes : " + quantityByFruit.get("Banane"));
		
		/*
		 * Vérifier la présence d'une clé dans la Map avec la méthode containsKey()
		 */
		if (quantityByFruit.containsKey("Citron")) {
			System.out.println("\r\nIl y a bien une clé nommée 'Citron' dans la map.");
		}
		if (!quantityByFruit.containsKey("Toto")) {
			System.out.println("\r\nIl n'y a pas de clé nommée 'Toto' dans la map.");
		}
		
		/*
		 * Supression d'un couple clé/valeur avec la méthode remove(),
		 * qui prend en paramètre la clé.
		 */
		quantityByFruit.remove("Kiwi");
		
		/*
		 * Afficher toutes le clés de la Map avec la méthode keySet()
		 */
		System.out.println("\r\nToutes les clés de la map : " + quantityByFruit.keySet());
		
		/*
		 * Map avec des chiens et leurs jouets
		 */
		Map<Dog, Set<Toy>> toysByDog = new HashMap<Dog, Set<Toy>>();
		
		Dog fido = new Dog("Fido", 8, 100, 30, DogRace.BERGER, null, 1234);
		Dog milou = new Dog("Milou", 5, 50, 10, DogRace.FOX_TERRIER, null, 5678);
		
		Set<Toy> fidoToys = new HashSet<Toy>();
		Set<Toy> milouToys = new HashSet<Toy>();
		
		Toy toy1 = new Toy("Balle");
		Toy toy2 = new Toy("Souris en plastique");
		Toy toy3 = new Toy("Branche");
		Toy toy4 = new Toy("Os");
		
		fidoToys.add(toy1);
		fidoToys.add(toy2);
		
		milouToys.add(toy3);
		milouToys.add(toy4);
		
		toysByDog.put(fido, fidoToys);
		toysByDog.put(milou, milouToys);
		
		Toy toy5 = new Toy("Truc machouillé");
		Toy toy6 = new Toy("Oeuf Fabergé");
		
		toysByDog.get(fido).add(toy5);
		toysByDog.get(milou).add(toy6);
		
		System.out.println("*** Parcours de la Map ***");
		
		System.out.println("\r\n--- keySet() ---");
		/*
		 * keySet() renvoie un set de toutes les clé de la map
		 */
		Set<Dog> dogs = toysByDog.keySet();
		for (Dog d : dogs) {
			System.out.println("\t" + d.getName());
		}
		
		System.out.println("\r\n--- get() ---");
		/*
		 * get() renvoie un Set de toutes les valeurs de la map
		 * associées à une clé donnée.
		 * Ici, de type Set<Toy>
		 */
		Set<Toy> toys = toysByDog.get(fido);
		System.out.println("Jouets de " + fido.getName() + " :");
		for (Toy t : toys) {
			System.out.println("\t" + t.getName());
		}
		Set<Toy> toys2 = toysByDog.get(milou);
		System.out.println("Jouets de " + milou.getName() + " :");
		for (Toy t : toys2) {
			System.out.println("\t" + t.getName());
		}
		
		System.out.println("\r\n--- entrySet() ---");
		/*
		 * entrySet() renvoie un Set qui encapsule les paires clé/valeur de la map.
		 */
		Set<Entry<Dog, Set<Toy>>> entries = toysByDog.entrySet();
		for (Entry<Dog, Set<Toy>> e : entries) {
			System.out.println("Jouets de " + e.getKey().getName());
			for (Toy t : e.getValue()) {
				System.out.println("\t" + t.getName());
			}
		}
	}
}
