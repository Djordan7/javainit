package fr.eql.ai110.java.init.demo.collection;

import java.util.HashSet;
import java.util.Set;

import fr.eql.ai110.java.init.demo.oop.Toy;

public class DemoHashSet {

	public static void main(String[] args) {
		
		/*
		 * Un set contient une collection d'éléments non ordonnés,
		 * et garantissant l'unicité des éléments contenus.
		 */
		
		/*
		 * Initialisation d'un set
		 */
		Set<Toy> toys = new HashSet<>();
		
		/*
		 * Ajouter des instances au set
		 */
		Toy toy1 = new Toy("Balle");
		Toy toy2 = new Toy("Souris en plastique");
		Toy toy3 = new Toy("Branche");
		
		toys.add(toy1);
		toys.add(toy2);
		toys.add(toy3);
		
		System.out.println("*** Obtenir la taille et les élément du set ***");
		/*
		 * Une instance déjà présente dans un set ne pourra y être rajoutée.
		 */
		System.out.println("\tTaille du set avant tentative d'ajout de doublon : " 
				+ toys.size());
		toys.add(toy3);
		System.out.println("\tTaille du set après tentative d'ajout de doublon : " 
				+ toys.size());
		
		/*
		 * Si les méthodes equals() et hashCode() ont été redéfinies dans la classe
		 * 'Toy', une égalité sur les attributs constituera un doublon.
		 */
		System.out.println("Si equals() et hashCode() redéfinis dans 'Toy', pas d'ajout :");
		System.out.println("\tTaille du set avant tentative d'ajout de doublon : " 
				+ toys.size());
		Toy toy4 = new Toy("Branche");
		toys.add(toy4);
		System.out.println("\tTaille du set après tentative d'ajout de doublon : " 
				+ toys.size());
		
		/*
		 * Les éléments présents dans un set ne sont pas ordonnés.
		 */
		System.out.println("\r\nContenu du set :");
		for (Toy t : toys) {
			System.out.println("\t" + t.getName());
		}
		
		System.out.println("\r\n*** Supprimer des éléments du set par référence ***");
		toys.remove(toy1);
		System.out.println("Le set ne contient plus que " + toys.size() 
		+ " jouet(s) :");
		for (Toy t : toys) {
			System.out.println(t.getName());
		}
		
		System.out.println("\r\n*** Vider le set ***");
		toys.removeAll(toys);
//		toys = new HashSet<>();
		if (toys.isEmpty()) {
			System.out.println("Le set de jouets est vide.");
		}
	}
}
