package fr.eql.ai110.java.init.demo.collection;

import java.util.ArrayList;
import java.util.List;

import fr.eql.ai110.java.init.demo.oop.Toy;

public class DemoForEach {

	public static void main(String[] args) {
		
		Toy toy1 = new Toy("Balle");
		Toy toy2 = new Toy("Souris en plastique");
		Toy toy3 = new Toy("Branche");
		
		System.out.println("*** Tableau ***");
		Toy[] toysInArray = {toy1, toy2, toy3};
		
		System.out.println("--- Boucle FOR ---");
		for (int i = 0; i < toysInArray.length; i++) {
			System.out.println(toysInArray[i].getName());
		}
		
		System.out.println("--- Boucle FOREACH ---");
		for (Toy t : toysInArray) {
			System.out.println(t.getName());
		}
		
		System.out.println("\r\n*** Liste ***");
		List<Toy> toysInList = new ArrayList<>();
		toysInList.add(toy1);
		toysInList.add(toy2);
		toysInList.add(toy3);
		
		System.out.println("--- Boucle FOR ---");
		for (int i = 0; i < toysInList.size(); i++) {
			System.out.println(toysInList.get(i).getName());
		}
		
		System.out.println("--- Boucle FOREACH ---");
		for (Toy t : toysInList) {
			System.out.println(t.getName());
		}
	}
}
