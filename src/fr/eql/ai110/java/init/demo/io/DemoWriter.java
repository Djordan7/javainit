package fr.eql.ai110.java.init.demo.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class DemoWriter {

	public static void main(String[] args) {
		
		System.out.println("*** FileWriter ***");
		try {
			FileWriter fw = new FileWriter("c:/DossierAI110/testFW.txt", false);
			fw.write("Hello AI110");
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\r\n*** BufferedWriter ***");
		try {
			FileWriter out = new FileWriter("c:/DossierAI110/testBW.txt", false);
			BufferedWriter bw = new BufferedWriter(out);
			bw.write("Hello again AI110");
			bw.newLine();
			bw.write("How are you doing ?");
			bw.close();
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
