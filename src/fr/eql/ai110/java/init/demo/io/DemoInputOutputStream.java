package fr.eql.ai110.java.init.demo.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class DemoInputOutputStream {

	public static void main(String[] args) {
		
		File file = new File("c:/DossierAI110/testStream.test");
		int[] bytes = {12000000, 28, 58, 45, 62};
		
		/*
		 * FileOutputStream permet d'écrire des octets (bytes) dans un fichier.
		 */
		try {
			FileOutputStream fos = new FileOutputStream(file);
			/*
			 * On écrit sous forme d'octets chacun des entiers contenus dans notre
			 * tableau d'entiers.
			 */
			for (int i : bytes) {
				fos.write(i);
			}
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 * FileInputStream permet de lire des octets dans un fichier.
		 */
		try {
			FileInputStream fis = new FileInputStream(file);
			int i = 0;
			/*
			 * La méthode read() de FileInputStream permet de lire les octets
			 * et renvoie -1 quand il n'y en a plus à lire.
			 */
			System.out.println("Les octets de mon fichier :");
			while ((i = fis.read()) != -1) {
				System.out.println(i);
			}
			fis.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
