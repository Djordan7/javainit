package fr.eql.ai110.java.init.demo.io;

import java.io.File;
import java.io.IOException;

public class DemoFile {

	public static void main(String[] args) {
		
		/*
		 * Instancier un File
		 */
		File file = new File("c:/DossierAI110");
		
		/*
		 * mkdir() permet de créer un dossier.
		 * Cette méthode renvoie true si le dossier a bien été créé.
		 */
		boolean isCreated = file.mkdir();
		if (isCreated) {
			System.out.println("Le dossier a bien été créé.");
		} else {
			System.out.println("Le dossier n'a pas été créé.");
		}
		
		/*
		 * Créer un fichier
		 */
		File file2 = new File("c:/DossierAI110/file.ai110");
		
		/*
		 * createNewFile() permet de créer un fichier.
		 * Cette méthode renvoie true si le fichier a bien été créé.
		 */

		try {
			isCreated = file2.createNewFile();
			if (isCreated) {
				System.out.println("Le fichier a bien été créé.");
			} else {
				System.out.println("Le fichier n'a pas été créé.");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * Création d'un fichier avec un chemin relatif
		 */
		File file3 = new File("file.toto");
		try {
			file3.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		/*
		 * getAbsolutePath() permet d'obtenir le chemin absolu d'un fichier
		 */
		System.out.println("Chemin absolu de mon fichier file3 : " + file3.getAbsolutePath());
		
		/*
		 * Vérifier mes droits de lecture ou d'écriture sur mon fichier.
		 */
		System.out.println("Je peux écrire sur file3 : " + file3.canWrite());
		System.out.println("Je peux lire file3 : " + file3.canRead());
		
		/*
		 * Créer une arborescence de dossiers (mkdirs() avec un 'S')
		 */
		File folders = new File("c:/DossierAI110/toto/titi/tata");
		isCreated = folders.mkdirs();
		System.out.println(isCreated ? "Dossierd créé" : "Dossier pas créé");
		
		/*
		 * Renommer un fichier
		 */
		File renamedFile3 = new File("file.tata");
		boolean isRenamed = file3.renameTo(renamedFile3);
		System.out.println(isRenamed ? "Fichier renommé" : "Fichier non renommé");
		
		/*
		 * Supprimer un fichier
		 */
		boolean isDeleted = renamedFile3.delete();
		System.out.println(isDeleted ? "Fichier supprimé" : "Fichier non supprimé");
		
		/*
		 * Afficher le contenu d'un dossier
		 */
		File root = new File("c:/");
		File[] content = root.listFiles();
		String type = "";
		for (File f : content) {
			if (f.isDirectory()) {
				type = "Dossier";
			} else if (f.isFile()){
				type = "Fichier";
			}
			System.out.println(f.getName() + " : " + type);
		}
	}
}
