package fr.eql.ai110.java.init.demo.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class DemoReader {

	public static void main(String[] args) {
		
		String content;
		
		System.out.println("*** FileReader ***");
		try {
			FileReader fr = new FileReader("c:/DossierAI110/testFW.txt");
			int character = 0;
			content = "";
			// -1 signifie qu'on a atteint la fin du fichier
			while ((character = fr.read()) != -1 ) {
				content += (char) character;
			}
			System.out.println("Contenu du fichier : " + content);
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\r\n*** BufferedReader ***");
		try {
			FileReader in = new FileReader("c:/DossierAI110/testBW.txt");
			BufferedReader br = new BufferedReader(in);
			
//			System.out.println("- caractère par caractère -");
//			content = "";
//			int character = 0;
//			while ((character = br.read()) != -1) {
//				content += (char) character;
//			}
//			System.out.println("Contenu du fichier :\r\n" + content);
//			
//			System.out.println("- ligne par ligne -");
			
			content = "";
			/*
			 * Le BufferedReader possède une méthode ready() qui renvoie true tant
			 * qu'il reste des éléments à lire dans le fichier.
			 */
			while (br.ready()) {
				content += br.readLine() + "\r\n";
			}
			System.out.println("Contenu du fichier :\r\n" + content);
			br.close();
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
