package fr.eql.ai110.java.init.demo.inheritence;

import java.util.Set;

import fr.eql.ai110.java.init.demo.enums.DogRace;
import fr.eql.ai110.java.init.demo.oop.Toy;

public class Dog extends Animal {

	private DogRace race;

	public Dog() {
		super();
	}
	
	/*
	 * Ce constructeur à 7 arguments appelle avec "super()" le constructeur
	 * de la superclasse (classe mère, ici "Animal") qui prend 6 arguments.
	 * Les 6 premiers arguments donnent des valeurs aux attributs de la classe mère.
	 * Le dernier argument sert à donner une valeur à l'attribut de la classe présente.
	 */
	public Dog(String name, int age, float size, float weight,
			Set<Toy> toys, int registrationNumber, DogRace race) {
		super(name, age, size, weight, toys, registrationNumber);
		this.race = race;
	}
	
	/*
	 * Ce constructeur à 5 arguments appelle avec "this()" l'autre constructeur
	 * de la classe prenant 7 arguments, en lui fournissant des valeurs
	 * prédéfinies pour les 2 derniers arguments.
	 */
	public Dog(String name, int age, float size, float weight, Set<Toy> toys) {
		this(name, age, size, weight, toys, 1234, DogRace.SPITZ);
	}
	
	public void bark() {
		System.out.println("Ouaf Ouaf !");
	}
	
	@Override
	public void fetchBall(int times) {
		System.out.println("Je rapporte " + times + " fois la baballe.");
	}
	
	public DogRace getRace() {
		return race;
	}
	public void setRace(DogRace race) {
		this.race = race;
	}
}
