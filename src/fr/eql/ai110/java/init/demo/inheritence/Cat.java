package fr.eql.ai110.java.init.demo.inheritence;

import java.util.Set;

import fr.eql.ai110.java.init.demo.enums.CatRace;
import fr.eql.ai110.java.init.demo.oop.Toy;

public class Cat extends Animal {

	private CatRace race;

	public Cat() {

	}
	
	public Cat(String name, int age, float size, float weight,
				Set<Toy> toys, int registrationNumber, CatRace race) {
		super(name, age, size, weight, toys, registrationNumber);
		this.race = race;
	}

	public void meow() {
		System.out.println("Miaaaaaou...");
	}
	
	@Override
	public void fetchBall(int times) {
		System.out.println("Je regarde rouler " + times + " fois la baballe.");
	}

	public CatRace getRace() {
		return race;
	}
	public void setRace(CatRace race) {
		this.race = race;
	}
}
