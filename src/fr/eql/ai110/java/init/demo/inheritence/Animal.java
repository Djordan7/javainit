package fr.eql.ai110.java.init.demo.inheritence;

import java.util.Set;

import fr.eql.ai110.java.init.demo.oop.Toy;

public abstract class Animal {

	private String name;
	private int age;
	private float size;
	private float weight;
	private Set<Toy> toys;
	private final int registrationNumber;
	
	public Animal() {
		registrationNumber = 1234;
	}
	
	public Animal(String name, int age, float size, float weight, Set<Toy> toys, int registrationNumber) {
		this.name = name;
		this.age = age;
		this.size = size;
		this.weight = weight;
		this.toys = toys;
		this.registrationNumber = registrationNumber;
	}

	public abstract void fetchBall(int times);
	
	public float sizeWeightRatio() {
		return size/weight;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public float getSize() {
		return size;
	}
	public void setSize(float size) {
		this.size = size;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public Set<Toy> getToys() {
		return toys;
	}
	public void setToys(Set<Toy> toys) {
		this.toys = toys;
	}
	public int getRegistrationNumber() {
		return registrationNumber;
	}
}
