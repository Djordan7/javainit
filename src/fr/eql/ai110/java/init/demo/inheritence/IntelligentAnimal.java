package fr.eql.ai110.java.init.demo.inheritence;

/*
 * Une interface est une sorte de classe au contenu entièrement abstrait.
 * Tous les attributs doivent être statiques, mais il est préférable
 * de ne pas en mettre.
 */
public interface IntelligentAnimal {

	/*
	 * Toutes les méthodes d'une interface sont abstraite et publique.
	 * Il n'est pas nécessaire d'écrire ces deux mots-clé.
	 */
	void hideItem(String item);
}
