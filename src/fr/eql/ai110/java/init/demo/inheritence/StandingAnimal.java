package fr.eql.ai110.java.init.demo.inheritence;

public interface StandingAnimal {

	void standUp();
}
