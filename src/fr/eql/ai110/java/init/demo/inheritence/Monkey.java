package fr.eql.ai110.java.init.demo.inheritence;

public class Monkey extends Animal implements IntelligentAnimal, StandingAnimal {

	@Override
	public void fetchBall(int times) {
		System.out.println("Je prends la baballe " + times + " fois et je la"
				+ "lance sur quelqu'un.");
	}

	@Override
	public void standUp() {
		System.out.println("Je me lève sur mes pattes arrières pour attraper une banane.");
	}

	@Override
	public void hideItem(String item) {
		System.out.println("Je cache le " + item + " dans un trou creusé au préalable.");
	}
}
