package fr.eql.ai110.java.init.demo.inheritence;

import java.util.HashSet;
import java.util.Set;

import fr.eql.ai110.java.init.demo.enums.CatRace;
import fr.eql.ai110.java.init.demo.enums.DogRace;
import fr.eql.ai110.java.init.demo.oop.Toy;

public class Launcher {

	public static void main(String[] args) {
		
		System.out.println("\t*** Dog : Fido ***");
		Dog fido = new Dog("Fido", 8, 100, 30, new HashSet<Toy>(), 1234, DogRace.FOX_TERRIER);
		fido.fetchBall(5);
		System.out.println(fido.sizeWeightRatio());
		fido.bark();
		
		System.out.println("\r\n\t*** Cat : Felix ***");
		Cat felix = new Cat("Felix", 12, 50, 6, new HashSet<Toy>(), 4568, CatRace.CHAT_DE_GOUTIERE);
		felix.fetchBall(5);
		System.out.println(felix.sizeWeightRatio());
		felix.meow();
		
		System.out.println("\r\n\t*** Animal : Spike ***");
		/*
		 * Je stocke la référence à une instance de type Dog dans une variable du type
		 * de sa superclasse (ici Animal).
		 * Je n'ai désormais plus accès qu'aux attributs et méthode d'un Animal, bien que
		 * l'instance en mémoire soit de type Dog.
		 */
		Animal spike = new Dog("Fido", 8, 100, 30, new HashSet<Toy>(), 1234, DogRace.FOX_TERRIER);
		spike.fetchBall(5);
		System.out.println(spike.sizeWeightRatio());
//		spike.bark();
		/*
		 * Je copie la référence de l'instance d'Animal spike dans une variable de type Dog.
		 * Je retrouve ainsi la possibilité d'accéder aux méthodes et attributs spécifiques
		 * à un objet de type Dog.
		 */
		Dog castedSpike = (Dog) spike;
		castedSpike.bark();
		
		System.out.println("\r\n\t*** Animal : Garfield ***");
		Animal garfield = new Cat("Garfield", 12, 50, 6, null, 4567, CatRace.BENGAL);
		System.out.println(garfield.sizeWeightRatio());
		garfield.fetchBall(5);
		Cat castedGarfield = (Cat) garfield;
		castedGarfield.meow();
		
		System.out.println("\r\n\t*** Boucle sur les animaux");
		Set<Animal> animals = new HashSet<Animal>();
		/*
		 * On peut stocker dans une collection contenant des objets de type Animal
		 * tous les objets d'un type hérité d'Animal.
		 */
		animals.add(fido);
		animals.add(felix);
		for (Animal animal : animals) {
			/*
			 * On vérifie le type de l'instance sortie de la collection.
			 */
			if (animal instanceof Dog) {
				System.out.println(animal.getName() + " est un chien.");
				Dog dog = (Dog) animal;
				dog.bark();
			}
			if (animal instanceof Cat) {
				System.out.println(animal.getName() + " est un chat.");
				Cat cat = (Cat) animal;
				cat.meow();
			}
		}
		
		Monkey monkey = new Monkey();
		monkey.setName("Saru");
		monkey.setSize(80);
		monkey.setWeight(20);
		monkey.hideItem("kiwi");
		monkey.standUp();
		animals.add(monkey);
		
		AnimalParade parade = new AnimalParade();
		parade.parade(animals);
	}
}
