package fr.eql.ai110.java.init.demo.keyword;

public class DemoStatic {

	/*
	 * Attributs statiques
	 */
	private static int staticNumber1 = 5;
	private static int staticNumber2;
	
	/*
	 * Bloc statique permettant d'effectuer des opérations dans un contexte statique
	 * (avant instanciation) une fois au moment du chargement de la classe.
	 */
	static {
		staticNumber2 = staticNumber1 * 2;
	}
	
	/*
	 * Attributs non statiques
	 */
	private int nonStaticNumber = 0;
	
	/*
	 * Méthode non statique
	 */
	public void whatInstanceAmI() {
		System.out.println("Je suis une instance de DemoStatic.");
	}
	
	/*
	 * Méthode statique
	 */
	public static void whatClassAmI() {
		System.out.println("Je suis DemoStatic. Voici un de mes attributs statiques : " 
				+ staticNumber1);
	}
	
	public static void main(String[] args) {
		
		System.out.println("*** Appel d'une méthode statique de Math");
		/*
		 * Appel d'une méthode statique.
		 * Ctrl + curseur sur la méthode + click gauche
		 * pour voir le code
		 * 
		 * Il n'est pas nécessaire de passer par une instance de l'objet
		 * pour y accéder.
		 * 
		 * Il suffit d'appeler directement la classe (commençant par une majuscule).
		 */
		
		System.out.println("Résultat d'une méthode statique de Math : " 
				+ Math.max(5, 10));
		
		System.out.println("\r\n*** Appel d'attributs statiques ***");
		System.out.println("staticNumber1 : " + staticNumber1);
		System.out.println("staticNumber2 : " + staticNumber2);
		
		/*
		 * La modification de la valeur d'attribut statique dans une instance d'un objet
		 * affecte également la valeur de cet attribut dans les autres instance de l'objet.
		 */
		DemoStatic demo1 = new DemoStatic();
		DemoStatic demo2 = new DemoStatic();
		demo1.staticNumber1 = 42;
		System.out.println("staticNumber1 de demo1 : " + demo1.staticNumber1);
		System.out.println("staticNumber1 de demo2 : " + demo2.staticNumber1);
		System.out.println("staticNumber1 de la classe : " + staticNumber1);
		
		/*
		 * Impossible d'appeler un attribut non statique dans un méthode main()
		 * qui est statique
		 */
//		System.out.println(nonStaticNumber);
		
		System.out.println("\r\n*** Appel d'une méthode statique de cette classe ***");
		/*
		 * J'accède àune méthode statique de cette classe sans instancier l'objet.
		 */
		whatClassAmI();
		
		System.out.println("\r\n*** Appel de méthodes statiques et non statiques "
				+ "d'une instance de cette classe ***");
		DemoStatic demo = new DemoStatic();
		demo.whatInstanceAmI();
		demo.whatClassAmI();
	}
}
