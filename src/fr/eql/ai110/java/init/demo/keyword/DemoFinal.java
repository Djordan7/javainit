package fr.eql.ai110.java.init.demo.keyword;

import fr.eql.ai110.java.init.demo.enums.DogRace;
import fr.eql.ai110.java.init.demo.oop.Dog;

public class DemoFinal {
	
	public static void main(String[] args) {
		
		/*
		 * Une variable déclarée finale ne peut être assignée qu'une fois.
		 * Elle ne peut plus ensuite pointer vers un autre emplacement mémoire.
		 * L'assignation ne doit pas nécéssairement se produire au moment
		 * de la déclaration.
		 */
		
		final int number;
		number = 0;
		final Dog milou;
		milou = new Dog("milou", 5, 50, 10, DogRace.FOX_TERRIER, null, 1234);
		
		/*
		 * On ne peut pas assigner une nouvelle valeur à une variable de type primitif.
		 */
//		number = 1;
		
		/*
		 * On peut modifier l'état (valeur des attributs) d'une instance déclarée en final.
		 */
		System.out.println("Age de Milou : " + milou.getAge());
		milou.setAge(6);
		System.out.println("Age de Milou modifié : " + milou.getAge());
		
		/*
		 * Mais on ne peut pas assigner à une variable de type objet une nouvelle instance
		 * (donc pointer vers un autre emplacement mémoire).
		 */
//		milou = new Dog("milou", 5, 50, 10, "Fox-Terrier", null);
	}
}
