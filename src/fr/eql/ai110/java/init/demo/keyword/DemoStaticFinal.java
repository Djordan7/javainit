package fr.eql.ai110.java.init.demo.keyword;

public class DemoStaticFinal {

	/*
	 * Un attribut statique et final est appelé une constante.
	 * Une constante s'écrit en majuscules, les mots séparés par
	 * un underscore (tiret bas).
	 */
	private static final int POSITIVE_NUMBER = 5;
	
	public static void main(String[] args) {
		
		System.out.println(POSITIVE_NUMBER);
	}
}
