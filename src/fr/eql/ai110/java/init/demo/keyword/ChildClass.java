package fr.eql.ai110.java.init.demo.keyword;

/*
 * Impossible d'étendre une classe finale.
 */
//public class ChildClass extends FinalClass {
public class ChildClass {

	public final void doSomething() {
		
	}
}
