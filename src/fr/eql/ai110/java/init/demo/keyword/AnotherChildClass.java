package fr.eql.ai110.java.init.demo.keyword;

public class AnotherChildClass extends ChildClass {

	/*
	 * Une méthode finale ne peut être redéfinie dans une classe fille.
	 */
//	@Override
//	public final void doSomething() {
//		
//	}
}
