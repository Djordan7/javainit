package fr.eql.ai110.java.init.demo.enums;

public enum CatRace {

	MAINE_COON, BENGAL, PERSAN, SIAMOIS, CHAT_DE_GOUTIERE
}
