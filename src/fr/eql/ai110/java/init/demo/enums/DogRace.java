package fr.eql.ai110.java.init.demo.enums;

public enum DogRace {
	FOX_TERRIER, SPITZ, LEVRIER, BERGER
}
