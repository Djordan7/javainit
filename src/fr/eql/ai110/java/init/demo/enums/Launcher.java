package fr.eql.ai110.java.init.demo.enums;

public class Launcher {

	public static void main(String[] args) {
		
		DogRace race;
		
		System.out.println("*** Affectation d'une variable de type enum ***");
		race = DogRace.SPITZ;
		System.out.println("Valeur de la variable : " + race);
		
		System.out.println("\r\n*** Transformer une chaîne de caractères en valeur d'enum");
		race = DogRace.valueOf("SPITZ");
		System.out.println("Valeur de la variable : " + race);
		
		System.out.println("\r\n*** Afficher toutes les valeurs nd'une enum ***");
		DogRace[] races = DogRace.values();
		for (DogRace d : races) {
			System.out.println(d);
		}
	}
}
