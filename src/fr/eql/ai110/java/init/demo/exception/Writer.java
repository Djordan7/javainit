package fr.eql.ai110.java.init.demo.exception;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Writer {
	
	private final static Logger logger = LogManager.getLogger();
	
	private static final String GOOD_FILE = "myFile.txt";
	private static final String BAD_FILE = "myFolder/myFile.txt";
	private static final String GOOD_CLASS = "fr.eql.ai110.java.init.demo.oop.Dog";
	private static final String BAD_CLASS = "Toto";
	
	private File myFile;
	private String simpleClassName;
	private Scanner myScanner;
	
	public void WriteClassNameInFile() throws ClassWritingException {
		myScanner = new Scanner(System.in);
		
		System.out.println(GOOD_FILE + " (1) ou " + BAD_FILE + " (2) ?");
		int fileChoice = Integer.parseInt(myScanner.nextLine());
		System.out.println(GOOD_CLASS + " (1) ou " + BAD_CLASS + " (2) ?");
		int classChoice = Integer.parseInt(myScanner.nextLine());
		
		String filePath = "";
		switch (fileChoice) {
		case 1 : 
			filePath = GOOD_FILE;
			break;
		case 2 :
			filePath = BAD_FILE;
			break;
		default :
			break;
		}
		
		String className = "";
		switch (classChoice) {
		case 1 : 
			className = GOOD_CLASS;
			break;
		case 2 :
			className = BAD_CLASS;
			break;
		default :
			break;
		}
		
		/*
		 * Chacune de ces trois méthodes privées peut jeter une 'checked exception' personnalisée
		 * nommée 'ClassWritingException'. Elle doit être gérée.
		 * Une des manières de gérer cette exception est de la rejeter à la méthode
		 * appelante en accolant le mot-clé 'throws' (avec un 's') et le nom de la classe 
		 * de l'exception à la signature de la méthode présente.
		 */
		createFile(filePath);
		retrieveSimpleClassName(className);
		writeSimpleName();
		
		myScanner.close();
	}
	
	/*
	 * La méthode createNewFile() de 'File' génère une 'checked exception', vérifiée à la
	 * compilation, donc devant être obligatoirement gérée afin que le code puisse compiler.
	 * Ici, elle est attrapée dans un bloc "try/catch" qui renvoie une autre 'checked exception'
	 * personnalisée qui sera passée à la méthode appelante.
	 */
	private void createFile(String filePath) throws ClassWritingException {
		myFile = new File(filePath);
		try {
			myFile.createNewFile();
		} catch (IOException e) {
			throw new ClassWritingException("La création du fichier '" + filePath + "' a échoué."
					+ "\r\nChoisissez un autre chemin valide.", e);
		}
	}
	
	/*
	 * La méthode forName() de 'Class' génère une 'checked exception', vérifiée à la
	 * compilation, donc devant être obligatoirement gérée afin que le code puisse compiler.
	 * Ici, elle est attrapée dans un bloc "try/catch" qui renvoie une autre 'checked exception'
	 * personnalisée qui sera passée à la méthode appelante.
	 */
	private void retrieveSimpleClassName(String className) throws ClassWritingException {

		try {
			Class myClass = Class.forName(className);
			simpleClassName = myClass.getSimpleName();
		} catch (ClassNotFoundException e) {
			throw new ClassWritingException("Il n'y a pas de classe ayant pour nom qualifié '" 
							+ className + "'.\r\nVeuillez e préciser un autre.", e);
		}
		
	}
	
	/*
	 * Les méthodes de 'FileWriter' génèrent une 'checked exception', vérifiée à la
	 * compilation, donc devant être obligatoirement gérée afin que le code puisse compiler.
	 * Ici, elle est attrapée dans un bloc "try/catch" qui renvoie une autre 'checked exception'
	 * personnalisée qui sera passée à la méthode appelante.
	 */
	private void writeSimpleName() throws ClassWritingException {
		System.out.println("Pointer vers le bon dossier ? Oui (1) ou Non (2)");
		File chosenFile = null;
		int choice = Integer.parseInt(myScanner.nextLine());
		switch (choice) {
		case 1:
			chosenFile = myFile;
			break;
		case 2 :
			chosenFile = new File("toto/titi");
			break;
		default:
			break;
		}
		
		try {
			FileWriter writer = new FileWriter(chosenFile, false);
			writer.write(simpleClassName);
			writer.close();
		} catch (IOException e) {
			throw new ClassWritingException("Une erreur s'est produite au moment de l'écriture "
					+ "du mot '" + simpleClassName + "' dans le fichier.", e);
		}
	}
}
