package fr.eql.ai110.java.init.demo.exception;

public class ClassWritingException extends Exception {

	private static final long serialVersionUID = 1L;

	public ClassWritingException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClassWritingException(String message) {
		super(message);
	}
}
