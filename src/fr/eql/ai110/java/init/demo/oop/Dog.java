package fr.eql.ai110.java.init.demo.oop;

import java.util.Set;

import fr.eql.ai110.java.init.demo.enums.DogRace;

public class Dog {

	/*
	 * Attributs (Variables d'instance)
	 */
	private String name;
	private int age;
	private float size;
	private float weight;
	private DogRace race;
	private Set<Toy> toys;
	/*
	 * Cet attribut est final.
	 * Il doit être assigné au plus tard au moment de la construction
	 * de l'instance.
	 */
	private final int registrationNumber;
	
	/*
	 * Constructeur (permet de créer une instance de cet objet)
	 * 
	 * Constructeur vide
	 */
	public Dog() {
		// On doit assigner ici une valeur à cet attribut final.
		registrationNumber = 0;
	}
	/*
	 * Constructeur surchargés
	 */
	public Dog(String name) {
		this.name = name;
		registrationNumber = 0;
	}
	
	public Dog(
			String name, 
			int age, 
			float size, 
			float weight, 
			DogRace race, 
			Set<Toy> toys,
			int registrationNumber
			) {
		this.name = name;
		this.age = age;
		this.size = size;
		this.weight = weight;
		this.race = race;
		this.toys = toys;
		this.registrationNumber = registrationNumber;
	}
	
	/*
	 * Méthodes
	 */
	public void bark() {
		System.out.println("Ouaf Ouaf !");
	}
	
	public void fetchBall(int times) {
		System.out.println("Je rapporte " + times + " fois la baballe");
	}
	
	public float sizeWeightRatio() {
		return size/weight;
	}
	
	/*
	 * Accesseurs et mutateurs (getters et setters)
	 */
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		if (!name.equals("toto")) {
			this.name = name;
		}
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public float getSize() {
		return size;
	}
	public void setSize(float size) {
		this.size = size;
	}
	public float getWeight() {
		return weight;
	}
	public void setWeight(float weight) {
		this.weight = weight;
	}
	public DogRace getRace() {
		return race;
	}
	public void setRace(DogRace race) {
		this.race = race;
	}
	public Set<Toy> getToys() {
		return toys;
	}
	public void setToys(Set<Toy> toys) {
		this.toys = toys;
	}
}
