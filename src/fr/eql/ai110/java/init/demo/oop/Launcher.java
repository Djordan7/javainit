package fr.eql.ai110.java.init.demo.oop;

import fr.eql.ai110.java.init.demo.enums.DogRace;

public class Launcher {

	public static void main(String[] args) {
		
		// Instanciation d'un objet avec son constructeur vide
		Dog laika = new Dog();
		// Instanciation d'un objet avec son constructeur surchargé
		Dog fido = new Dog("Fido", 8, 100, 30, DogRace.FOX_TERRIER, null, 1234);
		Toy anotherToy = new Toy("Autre joujou");
		
		// Affectation de ses attributs
		laika.setName("Laïka");
		System.out.println(laika.getName());
		laika.setName("toto");
		System.out.println(laika.getName());
//		laika.age = 12;
//		laika.size = 50;
//		laika.weight = 15;
//		laika.race = "Berger";
		
		// Affichage des attibuts
//		System.out.println("Laïka :");
//		System.out.println(laika.name);
//		System.out.println(laika.age);
//		System.out.println(laika.size);
//		System.out.println(laika.weight);
//		System.out.println(laika.race);
		
//		System.out.println("Fido :");
//		System.out.println(fido.name);
//		System.out.println(fido.age);
//		System.out.println(fido.size);
//		System.out.println(fido.weight);
//		System.out.println(fido.race);
		
		// Appel des méthodes
		laika.bark();
		laika.fetchBall(5);
		float ratio = laika.sizeWeightRatio();
		System.out.println(ratio);
		fido.fetchBall(10);
		
		System.out.println("*** redéfinition de toString ***");
		System.out.println(fido.toString());
	}
}
